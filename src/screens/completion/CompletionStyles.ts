import { StyleSheet } from "react-native";
import {
  HeadingSmallBold,
  HeadingXxSmallRegular,
  ButtonMediumBold,
} from "../../styles/typography";
import { colors } from "../../styles/color";

export const styles = StyleSheet.create({
  modalBackGround: {
    flex: 1,
    backgroundColor: colors.transparentColor,
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    width: 450,
    height: 402,
    backgroundColor: colors.secondary,
    borderRadius: 12,
    gap: 32,
    alignItems: "center",
  },
  modalHeader: {
    width: 450,
    height: 112,
    alignItems: "center",
    justifyContent: "space-between",
    paddingTop: 40,
    paddingRight: 24,
    paddingBottom: 0,
    paddingLeft: 24,
  },
  modalBody: {
    width: 450,
    height: 258,
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 56,
    paddingBottom: 40,
    gap: 40,
  },
  modalIconContainer: {
    width: 72,
    height: 72,
  },
  modalIcon: {
    width: 60,
    height: 60,
    left: 6,
    top: 6,
    color: "green",
  },
  modalTitle: {
    fontSize: HeadingSmallBold.size,
    lineHeight: HeadingSmallBold.lineHeight,
    color: colors.blackText,
  },
  modalTextContainer: {
    width: 338,
    height: 126,
    gap: 16,
    alignItems: "center",
  },
  modalText: {
    fontSize: HeadingXxSmallRegular.size,
    lineHeight: 34,
    color: colors.blackText,
  },
  modalButton: {
    width: 338,
    height: 52,
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 8,
    lineHeight: ButtonMediumBold.lineHeight,
  },
});

export default styles;
