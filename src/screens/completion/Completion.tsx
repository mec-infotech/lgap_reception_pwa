import React, { useEffect, useState } from "react";
import { View } from "react-native";
import styles from "./CompletionStyles";
import { HiraginoKakuText } from "../../components/StyledText";
import { AntDesign } from "@expo/vector-icons";
import { Button } from "../../components/basics/Button";

interface ModalPopupProps {
  closeModal: () => void;
}

const ModalPopup: React.FC<ModalPopupProps> = ({ closeModal }) => {
  const [seconds, setSeconds] = useState(10);
  useEffect(() => {
    let interval = setInterval(() => {
      if(seconds > 0) {
        setSeconds(seconds - 1);
      }
      if(seconds == 0) {
        clearInterval(interval);
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    }
  });
  return (
    <View style={styles.modalBackGround}>
      <View style={[styles.modalContainer]}>
        <View style={styles.modalHeader}>
          <View style={styles.modalIconContainer}>
            <AntDesign name="checkcircle" size={60} style={styles.modalIcon} />
          </View>
        </View>
        <View style={styles.modalBody}>
          <View style={styles.modalTextContainer}>
            <HiraginoKakuText style={styles.modalTitle}>
              受付が完了しました
            </HiraginoKakuText>
            <HiraginoKakuText style={styles.modalText} normal>
              {seconds}秒後に、自動で最初の画面に戻ります。
            </HiraginoKakuText>
          </View>
          <Button
            style={styles.modalButton}
            onPress={closeModal}
            text="最初の画面に戻る"
            type="ButtonLPrimary"
          />
        </View>
      </View>
    </View>
  );
};

export default ModalPopup;
