import React, { useState } from "react";
import { View } from "../../components/Themed";
import { HiraginoKakuText } from "../../components/StyledText";
import styles from "./CertificationStyles";
import ModalComponent from "../../components/basics/ModalComponent";
import { TextInput, Pressable } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { colors } from "../../styles/color";

type CertificationProps = {
  onFirstButtonPress?: () => void;
  onSecondButtonPress?: () => void;
  toggleModal: () => void;
};

export const Certification = (props: CertificationProps) => {
  const [userid, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [inputNotEmpty, setInputNotEmpty] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleInputChange = (text: string, isPassword: boolean = false) => {
    if (isPassword) {
      setPassword(text);
      setInputNotEmpty(userid.trim().length > 0 && text.trim().length > 0);
    } else {
      setUserId(text);
      setInputNotEmpty(text.trim().length > 0 && password.trim().length > 0);
    }
  };

  const handleByInput = () => {
    if( inputNotEmpty ){

    }
  };

  const handleLogin = () => {
    if (userid !== "mec" || password !== "1234") {
      setErrorMessage("IDまたはパスワードが正しくありません");
    } else {
      setErrorMessage("");
    }
  };

  return (
    <ModalComponent
      text="管理者画面"
      firstButtonText="キャンセル"
      secondButtonText="ログイン"
      onFirstButtonPress={props.onFirstButtonPress}
      onSecondButtonPress={handleLogin}
      modalHeight={errorMessage !== ""?434: 402}
      toggleModal={props.toggleModal}
      secondButtonDisable= {!inputNotEmpty}
    >
      <View style={styles.bodyContainer}>
        <HiraginoKakuText style={styles.bodyText} normal>
          管理者画面への移動は、ログインが必要です。
        </HiraginoKakuText>
        <View style={styles.inputContainer}>
          <View style={styles.inputItem}>
            <HiraginoKakuText style={styles.headingText}>ID</HiraginoKakuText>
            <TextInput style={styles.idInput} placeholder="ID"
              onChangeText={(text) => handleInputChange(text, false)}
              value={userid}
              placeholderTextColor={colors.placeholderTextColor} />
          </View>
          <View style={styles.inputItem}>
            <HiraginoKakuText style={styles.headingText}>パスワード</HiraginoKakuText>
            <View style={[styles.pwContainer]}>
              <TextInput style={styles.pwInput} 
                secureTextEntry={!showPassword}
                placeholder="パスワード"
                placeholderTextColor={colors.placeholderTextColor}
                value={password}
                onChangeText={(text) => handleInputChange(text, true)} />
              <Pressable
                style={styles.eyeIconContainer}
                onPress={togglePasswordVisibility}
                hitSlop={16}
              >
                {showPassword ? (
                  <Ionicons
                    name="eye"
                    size={24}
                    color="black"
                    style={styles.eyeIcon}
                  />
                ) : (
                  <Ionicons
                    name="eye-off"
                    size={24}
                    color="black"
                    style={styles.eyeIcon}
                  />
                )}
              </Pressable>
              <View style={styles.messageContainer}>
                {errorMessage !== "" && (
                  <HiraginoKakuText style={styles.errorMessage} normal>
                    {errorMessage}
                  </HiraginoKakuText>
                )}
              </View>
            </View>
          </View>
        </View>
      </View>
    </ModalComponent>
  );
};
