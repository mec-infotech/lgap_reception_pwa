import React, { ReactNode, useState } from 'react';
import { View, Text, Pressable, SafeAreaView, StatusBar } from 'react-native';
import { styles } from './EventDetailStyles';
import { Header } from '../../components/basics/header';
import { Footer } from '../../components/basics/footer';
import { HiraginoKakuText } from '../../components/StyledText';
import { useNavigation } from '@react-navigation/native';

export const EventDetail = () => {
  const [selectedOption, setSelectedOption] = useState('');
  const navigation = useNavigation();

  const handleSelectOption = (option: string) => {
    setSelectedOption(prevOption => prevOption === option ? '' : option);
  };

  const handleGoBack = () => {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={styles.detailMainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="詳細"
        buttonName=""
        hasButton={false}
      />
      <View style={styles.detailContainer}>
        <View style={styles.detailBodyContainer}>
          <View style={styles.detailOuterFrame1}>
            <HiraginoKakuText style={styles.subTitle}>出茂マラソン大会</HiraginoKakuText>
            <View style={styles.detailInnerFrame1}>
              <HiraginoKakuText style={styles.eventText} normal>イベント期間：</HiraginoKakuText>
              <HiraginoKakuText style={styles.eventText} normal>2023/05/12 〜 2023/05/12</HiraginoKakuText>
            </View>
          </View>

          <View style={styles.detailLineBreak}></View>

          <View style={styles.detailOuterFrame2}>
            <View style={styles.detailInnerFrame2}>
              <HiraginoKakuText style={styles.sentakuTitleText}>会場</HiraginoKakuText>
              <HiraginoKakuText style={styles.sentakuSubTitleText} normal>受付する会場を選択してください。</HiraginoKakuText>
            </View>

            <RadioPanel
              selected={selectedOption === 'A'}
              onPress={() => handleSelectOption('A')}
              radioBtnText="A会場"
            />

            <RadioPanel
              selected={selectedOption === 'B'}
              onPress={() => handleSelectOption('B')}
              radioBtnText="B会場"
            />

            <RadioPanel
              selected={selectedOption === 'C'}
              onPress={() => handleSelectOption('C')}
              radioBtnText="C会場"
            />

          </View>
        </View>
      </View>
      <Footer rightButtonText="受付開始" showNextIcon={false} onPressPrevious={handleGoBack}></Footer>
    </SafeAreaView>
  );
};

const RadioButton = (props: any) => {
  return (
    <View style={[{
      height: 24,
      width: 24,
      borderRadius: 12,
      borderWidth: 2,
      borderColor: '#000',
      alignItems: 'center',
      justifyContent: 'center',
    }, props.style]}>
      {props.selected
        ? <View style={{
          height: 10,
          width: 10,
          borderRadius: 6,
          backgroundColor: '#346DF4',
        }} />
        : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <View style={[styles.radioPanel, selected && styles.selectedRadioPanel]}>
      <Pressable onPress={onPress} style={styles.radioPressable}>
        <View style={styles.radioButtonIcon}>
          <RadioButton selected={selected} style={[styles.radioButton, selected && styles.selectedRadioButton]} />
        </View>
        <View style={styles.radioTextContainer}>
          <HiraginoKakuText style={styles.radioText}>{radioBtnText}</HiraginoKakuText>
        </View>
      </Pressable>
    </View>
  );
};
