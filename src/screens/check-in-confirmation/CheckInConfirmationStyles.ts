import { StyleSheet, Platform } from "react-native";
import { Dimensions } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { colors } from "../../styles/color";
import {
  HeadingMediumBold,
  HeadingXxSmallBold,
  HeadingXxSmallRegular,
  ButtonSmallBold,
} from "../../styles/typography";
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const isPortrait = height > width;

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
  bodyContainer: {
    alignItems: "center",
    backgroundColor: colors.bodyBackgroundColor,
    height: hp("78.1%"),
    paddingHorizontal: wp("3.35%"),
    paddingVertical: hp("3.85%"),
    gap: wp("2.02%"),
    flex: 1,
    justifyContent: "center",
    ...Platform.select({
      web: {
        paddingHorizontal: 0,
        paddingVertical: 0,
      },
    }),
  },
  innerBodyContainer: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: colors.gray,
    borderRadius: 16,
    width: wp("77.5%"),
    overflow: "hidden",
    borderBottomWidth: 1,
  },
  innerMainTitle: {
    height: hp("5.75%"),
  },
  innerMainTitleText: {
    fontSize: HeadingMediumBold.size,
    lineHeight: HeadingMediumBold.lineHeight,
    alignItems: "center",
    justifyContent: "center",
    color: colors.textColor,
  },
  bodyTitle: {
    backgroundColor: colors.neutralGrayColor,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: hp("8.15%"),
    ...Platform.select({
      ios: {
        height: isPortrait ? hp("7%") : hp("8.15%"),
      },
    }),
  },
  bodyTitleText: {
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
    height: 30,
    paddingHorizontal: 24,
    alignItems: "center",
    color: colors.textColor,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.secondary,
    paddingHorizontal: wp("2%"),
    gap: wp("1.35%"),
    paddingVertical: wp("1%"),
    height: hp("6.95%"),
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
  },
  rowAddress: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.secondary,
    paddingHorizontal: wp("2%"),
    gap: wp("1.35%"),
    paddingVertical: wp("1%"),
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
  },
  firstContent: {
    width: wp("14.4%"),
    gap: wp("0.65%"),
    justifyContent: "center",
  },
  firstContentAddress: {
    width: wp("14.4%"),
    gap: wp("0.65%"),
    justifyContent: "center",
  },

  secondContent: {
    width: wp("57.64%"),
  },
  secondContentAddress: {
    width: wp("57.64%"),
  },
  innerBodyBoldText: {
    alignItems: "center",
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
    fontWeight: "600",
    color: colors.textColor,
  },
  innerBodyText: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: HeadingXxSmallRegular.size,
    lineHeight: 34,
    color: colors.textColor,
  },
  iconStyle: {
    alignItems: "center",
    lineHeight: ButtonSmallBold.lineHeight,
    paddingTop: 3,
    paddingLeft: 3,
    width: 27,
    height: 27,
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    paddingHorizontal: wp("1.66%"),
    paddingVertical: wp("0.85%"),
    gap: wp("0.65%"),
    justifyContent: "center",
  },
  btnModify: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderRadius: 4,
    width: 184,
    height: 44,
    alignContent: "center",
    borderColor: colors.primary,
    fontSize: ButtonSmallBold.size,
    lineHeight: ButtonSmallBold.lineHeight,
  },
});

export default styles;