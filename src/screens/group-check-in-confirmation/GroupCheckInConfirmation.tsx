import React, { useState} from "react";
import {
  SafeAreaView,
  View,
  Dimensions,
  ScrollView,
  Pressable,
} from "react-native";
import styles from "./GroupCheckInConfirmationStyles";
import { StatusBar } from "react-native";
import { Header } from "../../components/basics/header";
import { HiraginoKakuText } from "../../components/StyledText";
import { Footer } from "../../components/basics/footer";
import { Button } from "../../components/basics/Button";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../../styles/color";

export const GroupCheckInConfirmation = () => {
  const screenWidth = Dimensions.get("window").width;
  const screenHeight = Dimensions.get("window").height;

  const [scrollEnabled, setScrollEnabled] = useState(false);

  const onLayoutHandler = (e: any) => {
    var { height } = e.nativeEvent.layout;

    if (height > 350) {
      setScrollEnabled(true);
    }
  };

  const [selectedDiv, setSelectedDiv] = useState<string | null>("divone");
  const handleDivSelect = (divId: string) => {
    setSelectedDiv(divId);
  };
  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content"></StatusBar>
      <Header titleName="受付内容確認" buttonName="受付をやめる"></Header>
      <View style={styles.bodyContainer}>
        <View style={styles.innerMainTitle}>
          <HiraginoKakuText style={styles.innerMainTitleText}>
            この内容で受付しますか？
          </HiraginoKakuText>
        </View>
        <View style={styles.outerContainer}>
          <View style={styles.sideScrollDiv}>
            <HiraginoKakuText style={styles.sideTitleText}>
              受付人数３人
            </HiraginoKakuText>

            <ScrollView
              style={styles.scrollableGroup}
              scrollEnabled={scrollEnabled}
            >
              <View style={styles.scrollableGp} onLayout={onLayoutHandler}>
                <Pressable onPress={() => handleDivSelect("divone")}>
                  <View
                    style={
                      selectedDiv === "divone"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>1.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 太郎
                    </HiraginoKakuText>
                  </View>
                </Pressable>
                <Pressable onPress={() => handleDivSelect("divtwo")}>
                  <View
                    style={
                      selectedDiv === "divtwo"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>2.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 二郎
                    </HiraginoKakuText>
                  </View>
                </Pressable>
                <Pressable onPress={() => handleDivSelect("divthree")}>
                  <View
                    style={
                      selectedDiv === "divthree"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>3.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 太郎ああああああああ
                    </HiraginoKakuText>
                  </View>
                </Pressable>
                <Pressable onPress={() => handleDivSelect("divfour")}>
                  <View
                    style={
                      selectedDiv === "divfour"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>4.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 太郎ああああ
                    </HiraginoKakuText>
                  </View>
                </Pressable>
                <Pressable onPress={() => handleDivSelect("divfive")}>
                  <View
                    style={
                      selectedDiv === "divfive"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>5.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 太郎ああああああああああああ
                    </HiraginoKakuText>
                  </View>
                </Pressable>
                <Pressable onPress={() => handleDivSelect("divsix")}>
                  <View
                    style={
                      selectedDiv === "divsix"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>6.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 太郎ああああ
                    </HiraginoKakuText>
                  </View>
                </Pressable>
                <Pressable onPress={() => handleDivSelect("divseven")}>
                  <View
                    style={
                      selectedDiv === "divseven"
                        ? styles.btnSideDivActive
                        : styles.btnSideDiv
                    }
                  >
                    <HiraginoKakuText style={styles.count}>7.</HiraginoKakuText>
                    <HiraginoKakuText
                      numberOfLines={1}
                      style={styles.sideText}
                      normal
                    >
                      出茂 太郎ああああ
                    </HiraginoKakuText>
                  </View>
                </Pressable>
              </View>
            </ScrollView>
          </View>
          {/* </View> */}
          {selectedDiv && (
            <View
              style={[
                styles.innerBodyContainer,
                selectedDiv === "divtwo"
                  ? styles.innerBodyContainerNot
                  : styles.innerBodyContainer,
              ]}
            >
              <View style={styles.bodyTitle}>
                <HiraginoKakuText style={styles.bodyTitleText}>
                  受付内容
                </HiraginoKakuText>
                <View style={styles.buttonContainer}>
                  <Button
                    text="内容を修正する"
                    type="ButtonMSecondary"
                    style={styles.btnModify}
                    icon={
                      <MaterialIcons
                        name="mode-edit"
                        size={24}
                        color={colors.primary}
                        style={styles.iconStyle}
                      />
                    }
                    iconPosition="behind"
                  ></Button>
                </View>
              </View>

              {selectedDiv === "divtwo" && (
                <View style={styles.row}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      あなたとの関係
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.secondContent}>
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      きょうだい
                    </HiraginoKakuText>
                  </View>
                </View>
              )}
              <View style={styles.row}>
                <View style={styles.firstContent}>
                  <HiraginoKakuText style={styles.innerBodyBoldText}>
                    お名前
                  </HiraginoKakuText>
                </View>
                <View style={styles.secondContent}>
                  <HiraginoKakuText style={styles.innerBodyText} normal>
                    出茂 太郎
                  </HiraginoKakuText>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.firstContent}>
                  <HiraginoKakuText style={styles.innerBodyBoldText}>
                    お名前（カナ）
                  </HiraginoKakuText>
                </View>
                <View style={styles.secondContent}>
                  <HiraginoKakuText style={styles.innerBodyText} normal>
                    イズモ タロウ
                  </HiraginoKakuText>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.firstContent}>
                  <HiraginoKakuText style={styles.innerBodyBoldText}>
                    生年月日
                  </HiraginoKakuText>
                </View>
                <View style={styles.secondContent}>
                  <HiraginoKakuText style={styles.innerBodyText} normal>
                    2000年01月02日
                  </HiraginoKakuText>
                </View>
              </View>
              {/* // 性別(Female/male/other) is optional. */}
              <View style={styles.row}>
                <View style={styles.firstContent}>
                  <HiraginoKakuText style={styles.innerBodyBoldText}>
                    性別
                  </HiraginoKakuText>
                </View>
                <View style={styles.secondContent}>
                  <HiraginoKakuText style={styles.innerBodyText} normal>
                    男性
                  </HiraginoKakuText>
                </View>
              </View>

              <View style={styles.row}>
                <View style={styles.firstContent}>
                  <HiraginoKakuText style={styles.innerBodyBoldText}>
                    郵便番号
                  </HiraginoKakuText>
                </View>

                <View style={styles.secondContent}>
                  <HiraginoKakuText style={styles.innerBodyText} normal>
                    515-0004
                  </HiraginoKakuText>
                </View>
              </View>

              <View style={styles.rowAddress}>
                <View style={styles.firstContentAddress}>
                  <HiraginoKakuText style={styles.innerBodyBoldText}>
                    住所
                  </HiraginoKakuText>
                </View>
                <View style={styles.secondContentAddress}>
                  <HiraginoKakuText style={styles.innerBodyText} normal>
                    三重県松阪市なんとか町11-2
                    マンション名102ああああああああああああああああああ
                  </HiraginoKakuText>
                </View>
              </View>
            </View>
          )}
        </View>
      </View>
      <Footer
        rightButtonText="受付する"
        hasPreviousButton={false}
        showNextIcon={false}
      ></Footer>
    </SafeAreaView>
  );
};

export default GroupCheckInConfirmation;
