import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },
  container: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "#f5f5f5",
  },
  leftSide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  rightSide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0, 0, 0, 0.4)",
    zIndex: 1,
  },
  camera: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: [{ translateX: -180 }, { translateY: -180 }],
    height: 360,
    width: 360,
    zIndex: 1,
  },
  corner: {
    position: "absolute",
    width: 30,
    height: 30,
    borderWidth: 3,
    borderRadius: 5,
    zIndex: 2,
    borderColor: "transparent",
  },

  topLeftCorner: {
    top: "50%",
    left: "50%",
    marginTop: -182,
    marginLeft: -182,
    borderTopColor: "white",
    borderLeftColor: "white",
  },

  topRightCorner: {
    top: "50%",
    left: "50%",
    marginTop: -182,
    marginLeft: 152,
    borderTopColor: "white",
    borderRightColor: "white",
  },

  bottomLeftCorner: {
    top: "50%",
    left: "50%",
    marginTop: 152,
    marginLeft: -182,
    borderBottomColor: "white",
    borderLeftColor: "white",
  },

  bottomRightCorner: {
    top: "50%",
    left: "50%",
    marginTop: 152,
    marginLeft: 152,
    borderBottomColor: "white",
    borderRightColor: "white",
  },
});
