import React from "react";
import { Pressable, StyleProp, TextStyle, ViewStyle } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";
import { useNavigation } from "@react-navigation/native";
 
type ScreenName = string;
 
interface TagLinkProps {
  text?: string;
  pressableStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  normal?: boolean;
  numberOfLines?: number;
}
 
export const TagLink: React.FC<TagLinkProps> = ({
  text,
  pressableStyle,
  textStyle,
  normal,
  numberOfLines,
}) => {
  const navigation = useNavigation();
 
  const url = "EventDetail";
 
  const handlePress = () => {
    navigation.navigate(url as never);
  };
 
  return (
    <Pressable onPress={handlePress} style={pressableStyle}>
      <HiraginoKakuText
        style={[
          { color: colors.primary, textDecorationLine: "underline" },
          textStyle,
        ]}
        normal={normal}
        numberOfLines={numberOfLines}
      >
        {text}
      </HiraginoKakuText>
    </Pressable>
  );
};