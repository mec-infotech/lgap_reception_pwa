export const fonts = {
  FontRegular: {
    fontFamily: "HiraginoKaku_GothicPro_Text",
    weight: "300",
  },
  FontBold: {
    fontFamily: "HiraginoKaku_GothicPro_Text_Bold",
    weight: "600",
  },
};
